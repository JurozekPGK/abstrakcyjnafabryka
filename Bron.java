/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package abstrakcyjnafabryka;

/**
 *
 * @author PC COMPUTER
 */
import java.util.Random;
public abstract class Bron {

    protected int ammoMag = 0;
    protected int maxMag=0;
    protected float accuracy=0;
    protected String model = null;
    protected String tekst = null;

    public Bron setAmmoMag(int ammoMag) throws Exception {
        if (ammoMag < maxMag && ammoMag > 0) {
            this.ammoMag = ammoMag;
        } else {
         throw new Exception("Błędna wartość!");
        
        }
        return this;
    }

    public Bron setModel(String model) {
        this.model = model;
        return this;
    }

    public int getAmmoMag() {
        return ammoMag;
    }

    public String getModel() {
        return model;
    }

    public Bron przeladuj() {
        
        ammoMag=maxMag;
        System.out.println("Przeladowano");
        return this;
    }

     public boolean strzelaj()
     {
       
          Random rn = new Random();
        
         for(;ammoMag>0;)
         {
             ammoMag--;
             System.out.println(tekst);
      
          
         if(rn.nextInt(101)<=(accuracy*100))
         {
             System.out.println("Trafiono!");
            return true; 
         }
         else
         {
           System.out.println("Pudło!");  
         }
         }
         return false;
        
     }
     
     public Bron Wyswietl()
     {
         System.out.println("Pozostało "+ammoMag+" pocisków.");
         return this;
     }

}
