/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstrakcyjnafabryka;

/**
 *
 * @author PC COMPUTER
 */
public class PistoletFactory extends AbstractWeaponFactory
{
 @Override
 public Pistolet getPistolet(String choice)
    {
       if(choice.equalsIgnoreCase("Colt")) 
       {
           return new Colt();
       }
         if(choice.equalsIgnoreCase("Magnum")) 
       {
           return new Magnum();
       }
        return null;
    }
    @Override
        public Karabin getKarabin(String choice)
        {
            return null;
        }    
}
