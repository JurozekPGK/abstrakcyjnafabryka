/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstrakcyjnafabryka;

/**
 *
 * @author PC COMPUTER
 */
public class FactoryProducer 
{
    public static AbstractWeaponFactory getFactory(String choice){
   
      if(choice.equalsIgnoreCase("KARABIN")){
         return new KarabinFactory();
         
      }else if(choice.equalsIgnoreCase("PISTOLET")){
         return new PistoletFactory();
      }
      
      return null;
   }
    
}
